# abstract_iga_client

abstract_iga_client is a Python 3.x library for accessing IGA data
uniformly from all the universities. It uses the specific implementation
for each institution.

It is tested with python version 3.7, 3.8, and 3.9.

## Usage

Add dependency in your main project pyproject.toml:

```toml
abstract-iga-client = {git = "https://bitbucket.usit.uio.no/scm/bottint/abstract-iga-client.git", rev = "v0.5.0"}
```

Then use it like this:

```python
from abstract_iga_client.iga_abstract import IgaAbstract

client = IgaAbstract.get_iga_client(
    'cerebrum@uio',   # possible values: cerebrum@uio, scim@uit, cerebrum@ntnu, scim@uib
    'http://localhost/',
    headers={'X-Gravitee-Api-Key': 'abababab-abab-abab-abab-abababababab'})

result = client.get_person_by_id('username')

print("Test {}".format(result.display_name))
```
