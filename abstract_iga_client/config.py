from abc import ABC
from typing import Optional, Dict, Any, Tuple, Union, TYPE_CHECKING

import pydantic
from scim2_client import Config as Scim2ClientConfig
from typing_extensions import Literal, Annotated

if TYPE_CHECKING:
    AnyHttpUrl = str
else:
    AnyHttpUrl = pydantic.AnyHttpUrl


class ClientConfigABC(ABC, pydantic.BaseModel):
    pass


class UibScimRiConfig(ClientConfigABC, Scim2ClientConfig):
    iga_impl: Literal["scim-ri@uib"]
    items_per_page: Optional[int]
    # SCIM supports complex filters: active eq true and firstName sw "eg"
    # Complex filters stopped working in UiB's api for a while, but should be
    # working now.  Set this to false to use manual filtering if the api stops
    # working again
    can_use_complex_filters: bool = True


class UitScimRiConfig(ClientConfigABC, Scim2ClientConfig):
    iga_impl: Literal["scim-ri@uit"]
    items_per_page: Optional[int]
    # SCIM supports complex filters: active eq true and firstName sw "eg"
    # Complex filters stopped working in UiT's api for a while, but should be
    # working now.  Set this to false to use manual filtering if the api stops
    # working again
    can_use_complex_filters: bool = True


class CerebrumConfig(ClientConfigABC):
    url: AnyHttpUrl
    headers: Optional[Dict[str, Any]]
    rewrite_url: Optional[Tuple[str, str]]


class UioCerebrumConfig(CerebrumConfig):
    iga_impl: Literal["cerebrum@uio"]


class NtnuTiaConfig(CerebrumConfig):
    iga_impl: Literal["cerebrum@ntnu"]


ClientConfig = Annotated[
    Union[UibScimRiConfig, UioCerebrumConfig, UitScimRiConfig, NtnuTiaConfig],
    pydantic.Field(discriminator="iga_impl"),
]


def parse_client_config(**kwargs: Any) -> ClientConfig:
    return pydantic.parse_obj_as(ClientConfig, kwargs)  # type: ignore[arg-type]
