from abstract_iga_client.iga_cerebrum_uio import IgaCerebrumUio, OwnerTypeEnum

from typing import Optional, Set, Iterable

from abstract_iga_client.iga_result import (
    IgaAccount,
    IgaPerson,
    IgaResult,
)


class IgaCerebrumUit(IgaCerebrumUio):
    SOURCE_SYSTEM_SAP = "PAGA"
    SAP_ID_TYPE_EMPLOYEE = "pagaEmployeeNumber"
    STRIP_LEADING_ZEROS_FROM_EMPLOYEE_NUMBER = False

    def get_igaperson_by_username(
        self, username: str, fields_to_include: Set[str]
    ) -> Optional[IgaPerson]:
        """Use IgaPerson.__fields__ if you want all fields"""
        if not fields_to_include:
            raise ValueError("fields_to_include can't be empty")

        account = self.client.get_account(username)
        if account is None or account.owner.type != OwnerTypeEnum.person:
            return None

        iga_person = IgaPerson()

        if {"accounts", "primary_email", "feide_id"} & fields_to_include:
            person_accounts = self.client.get_person_accounts(account.owner.id)
            # map accounts to list of tuples with (account_type, username)
            accounts = list(
                map(
                    lambda x: IgaAccount(type="primary", username=x.name)
                    if x.primary
                    else IgaAccount(type="secondary", username=x.name),
                    person_accounts or (),
                )
            )
            iga_person.accounts = accounts
            try:
                prim_acc_info = next(filter(lambda x: x.type == "primary", accounts))
                # Set attributes if we already have info
                if prim_acc_info.username == account.name:
                    if account.primary_email:
                        iga_person.primary_email = account.primary_email
                    iga_person.feide_id = account.name + "@uit.no"
                # Fetch if we didn't have it and specified it is wanted
                else:
                    if {"primary_email", "feide_id"} & fields_to_include:
                        primary_acc = self.client.get_account(prim_acc_info.username)
                        if primary_acc is None:
                            raise ValueError
                        if primary_acc.primary_email:
                            iga_person.primary_email = primary_acc.primary_email
                        iga_person.feide_id = primary_acc.name + "@uit.no"
            except StopIteration:
                pass

        if {"sap_person_id", "fs_person_id", "greg_person_id"} & fields_to_include:
            # Both use externalids endpoint, and result is cached giving both with one
            # call
            sap_person_id = self.get_employee_nr(account.owner.id)
            if sap_person_id:
                iga_person.sap_person_id = sap_person_id
            fs_person_id = self.get_student_nr(account.owner.id)
            if fs_person_id:
                iga_person.fs_person_id = fs_person_id
            greg_person_id = self.get_greg_person_id(account.owner.id)
            if greg_person_id:
                iga_person.greg_person_id = greg_person_id

        if {"iga_person_id", "display_name"} & fields_to_include:
            person = self.client.get_person(account.owner.id)
            if person is None:
                raise ValueError
            iga_person.iga_person_id = str(person.id)
            disp_name = self.get_display_name(person, return_model=True)
            if disp_name:
                iga_person.display_name = disp_name

        if {"phone_numbers"} & fields_to_include:
            conts = self.client.get_person_contact(account.owner.id)
            if conts:
                iga_person.phone_numbers = self._contacts2phone_numbers(conts)

        return iga_person

    def get_igaperson_by_personid(
        self, person_id: int, fields_to_include: Set[str]
    ) -> Optional[IgaPerson]:
        """Use IgaPerson.__fields__ if you want all fields"""
        if not fields_to_include:
            raise ValueError("fields_to_include can't be empty")

        iga_person = IgaPerson(iga_person_id=person_id)  # type: ignore[arg-type]

        if {"accounts", "primary_email", "feide_id"} & fields_to_include:
            person_accounts = self.client.get_person_accounts(person_id)
            # map accounts to list of tuples with (account_type, username)
            accounts = list(
                map(
                    lambda x: IgaAccount(type="primary", username=x.name)
                    if x.primary
                    else IgaAccount(type="secondary", username=x.name),
                    person_accounts or (),
                )
            )
            iga_person.accounts = accounts
            if {"primary_email", "feide_id"} & fields_to_include:
                try:
                    prim_acc_info = next(
                        filter(lambda x: x.type == "primary", accounts)
                    )
                    primary_acc = self.client.get_account(prim_acc_info.username)
                    if primary_acc is None:
                        raise ValueError
                    if primary_acc.primary_email:
                        iga_person.primary_email = primary_acc.primary_email
                    iga_person.feide_id = primary_acc.name + "@uit.no"
                except StopIteration:
                    pass

        if {"sap_person_id", "fs_person_id", "greg_person_id"} & fields_to_include:
            # Both use externalids endpoint, and result is cached giving both with one
            # call
            sap_person_id = self.get_employee_nr(person_id)
            if sap_person_id:
                iga_person.sap_person_id = sap_person_id
            fs_person_id = self.get_student_nr(person_id)
            if fs_person_id:
                iga_person.fs_person_id = fs_person_id
            greg_person_id = self.get_greg_person_id(person_id)
            if greg_person_id:
                iga_person.greg_person_id = greg_person_id

        if {"display_name"} & fields_to_include:
            person = self.client.get_person(person_id)
            if person is None:
                raise ValueError
            disp_name = self.get_display_name(person, return_model=True)
            if disp_name:
                iga_person.display_name = disp_name

        if {"phone_numbers"} & fields_to_include:
            conts = self.client.get_person_contact(person_id)
            if conts:
                iga_person.phone_numbers = self._contacts2phone_numbers(conts)

        return iga_person

    def get_all_greg_persons(self) -> Iterable[IgaResult]:
        raise NotImplementedError
