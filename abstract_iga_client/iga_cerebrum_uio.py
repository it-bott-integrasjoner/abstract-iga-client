from __future__ import annotations
from functools import lru_cache
from typing import List, Tuple, Optional, Iterator, Set, Union, overload, Any, Iterable

from typing_extensions import Literal
import requests
from cerebrum_client import CerebrumClient
from cerebrum_client.models import (
    Account,
    Affiliation as CerebrumAffiliation,
    Consent,
    Contact,
    NameVariantEnum,
    OU,
    OwnerTypeEnum,
    Person,
    PersonExternalId,
    GroupListItem,
    Group,
)

from abstract_iga_client.config import CerebrumConfig
from abstract_iga_client.iga_abstract import IgaAbstract
from abstract_iga_client.iga_result import (
    Affiliation,
    DisplayName,
    IgaAccount,
    IgaPerson,
    IgaResult,
    PhoneNumber,
    IgaGroup,
)


class IgaCerebrumUio(IgaAbstract):
    PERSON_NAME_SOURCE = "Cached"
    PERSON_NAME_VARIANT_FULL = NameVariantEnum.full
    PERSON_NAME_VARIANT_FIRST = NameVariantEnum.first
    PERSON_NAME_VARIANT_LAST = NameVariantEnum.last
    SOURCE_SYSTEM_SAP = "DFO_SAP"
    SOURCE_SYSTEM_FS = "FS"
    SOURCE_SYSTEM_GREG = "GREG"
    SAP_ID_TYPE_EMPLOYEE = "dfoPersonId"
    FS_ID_TYPE_STUDENT = "studentNumber"
    GREG_ID_TYPE_GREG_PERSON_ID = "gregPersonId"
    STRIP_LEADING_ZEROS_FROM_EMPLOYEE_NUMBER = True
    ENTITY_TYPE_ACCOUNT = "account"

    def __init__(
        self, session: Optional[requests.Session] = None, *, config: CerebrumConfig
    ) -> None:
        super().__init__(session)
        self.client = CerebrumClient(
            url=config.url,
            use_sessions=self.session or False,
            headers=config.headers,
            rewrite_url=config.rewrite_url,
        )
        # lru_cache declared directly on class methods leads to memory leaks
        # https://stackoverflow.com/questions/14946264/python-lru-cache-decorator-per-instance/14946506#14946506
        self._get_person_external_ids = lru_cache()(self._get_person_external_ids)  # type: ignore[method-assign]
        self._get_ou = lru_cache()(self._get_ou)  # type: ignore[method-assign]

    def _get_ou(self, ou_id: int) -> Optional[OU]:
        return self.client.get_ou(ou_id)

    def get_person_by_id(self, person_id: int) -> Optional[IgaResult]:
        """
        Get person with id in UiOCerebrum

        :param person_id: (entity_)id of the person object in cerebrum
        :return: IgaResult object
        """
        person = self.client.get_person(person_id)
        if person is None:
            return None
        account = self.client.get_person_primary_account(person_id)
        if account is None:
            return None
        employee_nr = self.get_employee_nr(person_id)
        student_number = self.get_student_nr(person_id)
        greg_person_id = self.get_greg_person_id(person_id)
        affs = self.client.get_person_affiliations(person_id)
        affiliations: Iterator[CerebrumAffiliation]
        if affs is None:
            self.logger.warning("get_person_affiliations returned None")
            affiliations = iter(())
        else:
            affiliations = affs

        result = self.transform_result(
            person,
            account,
            affiliations,
            employee_number=employee_nr,
            student_number=student_number,
            greg_person_id=greg_person_id,
        )
        if result is None:
            self.logger.warning(
                "Error getting data from iga for person_id %r", person_id
            )
        return result

    def get_person_by_username(self, username: str) -> Optional[IgaResult]:
        """
        :param username: Username in cerebrum
        :return: IgaResult object
        """

        account = self.client.get_account(username)
        if not account:
            self.logger.debug("No account found with username %s", username)
            return None
        if account.owner.type != OwnerTypeEnum.person:
            self.logger.debug("Owner of account %s is not a person", username)
            return None
        person_id = account.owner.id
        person = self.client.get_person(person_id)
        if person is None:
            return None
        employee_nr = self.get_employee_nr(person_id)
        student_number = self.get_student_nr(person_id)
        affiliations = self.client.get_person_affiliations(person_id)
        greg_person_id = self.get_greg_person_id(person_id)

        result = self.transform_result(
            person,
            account,
            affiliations,
            employee_number=employee_nr,
            student_number=student_number,
            greg_person_id=greg_person_id,
        )
        if result is None:
            self.logger.debug(
                "Error getting data from iga for username {}".format(username)
            )
        return result

    def get_person_by_employee_nr(self, employee_nr: str) -> Optional[IgaResult]:
        # TODO: Find a better way to deal with the fact that Cerebrum strips away
        #  zeros from the front of dfo pids
        if self.STRIP_LEADING_ZEROS_FROM_EMPLOYEE_NUMBER:
            employee_nr = employee_nr.lstrip("0")
        person_search_result = self.client.search_person_external_ids(
            source_system=self.SOURCE_SYSTEM_SAP,
            id_type=self.SAP_ID_TYPE_EMPLOYEE,
            external_id=employee_nr,
        )
        # Should be only one match so return first result in generator
        for result in person_search_result:
            person_id = result.person_id
            person = self.client.get_person(person_id)
            if person is None:
                return None
            account = self.get_primary_account(person_id)
            if account is None:
                return None
            student_number = self.get_student_nr(person_id)
            affiliations = self.client.get_person_affiliations(person_id)
            greg_person_id = self.get_greg_person_id(person_id)

            iga_result = self.transform_result(
                person,
                account,
                affiliations,
                employee_number=employee_nr,
                student_number=student_number,
                greg_person_id=greg_person_id,
            )
            if iga_result is None:
                self.logger.debug(
                    "Error getting data from iga for employee_nr %r", employee_nr
                )
            return iga_result

        self.logger.warning(
            "Error getting data from iga for employee_nr %r", employee_nr
        )
        return None

    def get_person_by_fs_id(self, fs_id: str) -> Optional[IgaResult]:
        person_search_result = self.client.search_person_external_ids(
            source_system=self.SOURCE_SYSTEM_FS,
            id_type=self.FS_ID_TYPE_STUDENT,
            external_id=fs_id,
        )
        # Should be only one match so return first result in generator
        for result in person_search_result:
            person_id = result.person_id
            person = self.client.get_person(person_id)
            if person is None:
                return None
            account = self.get_primary_account(person_id)
            if account is None:
                return None
            sap_id = self.get_employee_nr(person_id)
            greg_person_id = self.get_greg_person_id(person_id)
            affiliations = self.client.get_person_affiliations(person_id)

            iga_result = self.transform_result(
                person,
                account,
                affiliations,
                employee_number=sap_id,
                student_number=fs_id,
                greg_person_id=greg_person_id,
            )
            if iga_result is None:
                self.logger.warning(
                    "Error getting data from iga for {} {}".format(
                        self.FS_ID_TYPE_STUDENT, fs_id
                    )
                )
            return iga_result

        self.logger.warning(
            "Error getting data from iga for {} {}".format(
                self.FS_ID_TYPE_STUDENT, fs_id
            )
        )
        return None

    def get_person_by_greg_person_id(self, value: int) -> Optional[IgaResult]:
        source_system = self.SOURCE_SYSTEM_GREG
        id_type = self.GREG_ID_TYPE_GREG_PERSON_ID
        person_search_result = self.client.search_person_external_ids(
            source_system=source_system,
            id_type=id_type,
            external_id=value,
        )
        # Should be only one match so return first result in generator
        for result in person_search_result:
            person_id = result.person_id
            person = self.client.get_person(person_id)
            if person is None:
                return None
            account = self.get_primary_account(person_id)
            if account is None:
                return None
            sap_id = self.get_employee_nr(person_id)
            student_number = self.get_student_nr(person_id)
            affiliations = self.client.get_person_affiliations(person_id)

            iga_result = self.transform_result(
                person,
                account,
                affiliations,
                employee_number=sap_id,
                student_number=student_number,
                greg_person_id=value.__str__(),
            )
            if iga_result is None:
                self.logger.warning(
                    "Error getting data from iga for %s %s %s",
                    source_system,
                    id_type,
                    value,
                )
            return iga_result

        self.logger.warning(
            "Error getting data from iga for %s %s %s",
            source_system,
            id_type,
            value,
        )
        return None

    def get_primary_account(self, person_id: int) -> Optional[Account]:
        accounts = self.client.get_person_accounts(person_id)
        if accounts is None:
            return None
        for account in accounts:
            # Skal vi ta høyde for at en ansatt ikke har en primary account?
            if account.primary:
                return self.client.get_account(account.name)
        return None

    def get_groups(self, username: str) -> Optional[List[IgaGroup]]:
        groups = self.client.get_account_memberships(username)
        if groups is None:
            return None
        return list(map(self.create_iga_group, groups))

    def get_group_members(self, group_name: str) -> List[str]:
        if not isinstance(group_name, str):
            raise TypeError
        group_name = group_name.strip()
        if not group_name:
            raise ValueError("group_name can't be empty")
        members = self.client.list_group_members(group_name)
        if members is None:
            # Method is wrongly typed, it never returns None
            raise TypeError("Unreachable")
        return [
            x.name
            for x in members
            if x.type == self.ENTITY_TYPE_ACCOUNT and x.name is not None
        ]

    def get_all_persons(self) -> Union[List[IgaResult], None]:
        # UiO cerebrum client støtter ikke hent alle personer så vidt jeg kan se?
        raise NotImplementedError

    def get_all_employees(self) -> Union[List[IgaResult], None]:
        raise NotImplementedError

    def transform_result(
        self,
        person: Person,
        account: Account,
        affiliations: Optional[Iterator[CerebrumAffiliation]],
        *,
        employee_number: Optional[str],
        student_number: Optional[str],
        greg_person_id: Optional[str],
    ) -> Optional[IgaResult]:
        if person is None or account is None:
            raise TypeError
        affiliations_list: List[CerebrumAffiliation] = (
            list(affiliations) if affiliations else []
        )
        org_import_id: Optional[str] = None
        for affiliation in affiliations_list:
            if affiliation.affiliation == "ANSATT":
                ou = self._get_ou(affiliation.ou.id)
                org_import_id = ou.stedkode[0:6] if ou else None
            elif affiliation.affiliation == "STUDENT":
                ou = self._get_ou(affiliation.ou.id)
                org_import_id = ou.stedkode[0:4] if ou else None
            break  # The first affiliation in list, is the main affiliation
        # Transform into standard result object
        return IgaResult(
            iga_person_id=str(person.id),
            sap_person_id=employee_number,
            fs_person_id=student_number,
            username=account.name,
            email=account.primary_email,
            display_name=self.get_display_name(person),
            affiliations=[Affiliation() for _ in affiliations_list],
            org_import_id=org_import_id,
            greg_person_id=greg_person_id,
        )

    @overload
    def get_display_name(
        self, person: Person, return_model: Literal[True]
    ) -> Optional[DisplayName]:
        pass

    @overload
    def get_display_name(
        self, person: Literal[None]
    ) -> Tuple[Literal[None], Literal[None]]:
        pass

    @overload
    def get_display_name(
        self, person: Literal[None], return_model: bool
    ) -> Tuple[Literal[None], Literal[None]]:
        pass

    @overload
    def get_display_name(
        self, person: Person, return_model: Literal[False]
    ) -> Tuple[Optional[str], Optional[str]]:
        pass

    @overload
    def get_display_name(self, person: Person) -> Tuple[Optional[str], Optional[str]]:
        pass

    def get_display_name(
        self, person: Optional[Person], return_model: bool = False
    ) -> Union[Optional[DisplayName], Tuple[Optional[str], Optional[str]]]:
        """Fetch the display name from the Person object

        Uses PERSON_NAME_SOURCE, PERSON_NAME_VARIANT_FIRST and PERSON_NAME_VARIANT_LAST
        to find the correct first and last name of the person.

        if return_model is True you get either a DisplayName object with first and last
        name set, or None
        if return_model is False you get a tuple with two elements where either can be
        None or a string
        """
        if person is None or person.names is None:
            return None, None
        # In UiO cerebrum client person has a list of names.
        # Loop through these names and pick the one with the right source
        first, last = None, None
        for name in person.names:
            if name.source_system == self.PERSON_NAME_SOURCE:
                if name.variant == self.PERSON_NAME_VARIANT_FIRST:
                    first = name.name
                elif name.variant == self.PERSON_NAME_VARIANT_LAST:
                    last = name.name
                if first and last:
                    break
        if return_model:
            if first and last:
                return DisplayName(first=first, last=last)
            else:
                return None
        else:
            return first, last

    def _get_person_external_ids(
        self, person_id: int
    ) -> Optional[List[PersonExternalId]]:
        """
        Cache for external ids to prevent separate calls for student id and employee id.
        """
        # unpack the generator so that the cached values aren't "used up"
        person_ids = self.client.get_person_external_ids(person_id)
        if person_ids is None:
            return None
        return list(person_ids)

    def get_employee_nr(self, person_id: int) -> Optional[str]:
        return self._get_external_id(
            person_id=person_id,
            source_system=self.SOURCE_SYSTEM_SAP,
            id_type=self.SAP_ID_TYPE_EMPLOYEE,
        )

    def get_student_nr(self, person_id: int) -> Optional[str]:
        return self._get_external_id(
            person_id=person_id,
            source_system=self.SOURCE_SYSTEM_FS,
            id_type=self.FS_ID_TYPE_STUDENT,
        )

    def get_greg_person_id(self, person_id: int) -> Optional[str]:
        return self._get_external_id(
            person_id=person_id,
            source_system=self.SOURCE_SYSTEM_GREG,
            id_type=self.GREG_ID_TYPE_GREG_PERSON_ID,
        )

    def _get_external_id(
        self, *, person_id: int, source_system: str, id_type: str
    ) -> Optional[str]:
        external_ids = self._get_person_external_ids(person_id)
        if external_ids is None:
            return None

        for ext_id in external_ids:
            if ext_id.source_system == source_system and ext_id.id_type == id_type:
                return ext_id.external_id

        return None

    def get_person_consents(self, person_id: int) -> Optional[Iterator[Consent]]:
        return self.client.get_person_consents(person_id)

    def _contacts2phone_numbers(
        self, contact_iterator: Iterator[Contact]
    ) -> List[PhoneNumber]:
        return list(
            filter(
                None,
                map(
                    lambda x: PhoneNumber(
                        type=x.type, source=x.source_system, value=x.value
                    ),
                    contact_iterator,
                ),
            )
        )

    def get_igaperson_by_username(
        self, username: str, fields_to_include: Set[str]
    ) -> Optional[IgaPerson]:
        """Use IgaPerson.__fields__ if you want all fields"""
        if not fields_to_include:
            raise ValueError("fields_to_include can't be empty")

        account = self.client.get_account(username)
        if account is None or account.owner.type != OwnerTypeEnum.person:
            return None

        iga_person = IgaPerson()

        if {"accounts", "primary_email", "feide_id"} & fields_to_include:
            person_accounts = self.client.get_person_accounts(account.owner.id)
            # map accounts to list of tuples with (account_type, username)
            accounts = list(
                map(
                    lambda x: IgaAccount(type="primary", username=x.name)
                    if x.primary
                    else IgaAccount(type="secondary", username=x.name),
                    person_accounts or (),
                )
            )
            iga_person.accounts = accounts
            try:
                prim_acc_info = next(filter(lambda x: x.type == "primary", accounts))
                # Set attributes if we already have info
                if prim_acc_info.username == account.name:
                    if account.primary_email:
                        iga_person.primary_email = account.primary_email
                    iga_person.feide_id = account.name + "@uio.no"
                # Fetch if we didn't have it and specified it is wanted
                else:
                    if {"primary_email", "feide_id"} & fields_to_include:
                        primary_acc = self.client.get_account(prim_acc_info.username)
                        if primary_acc is None:
                            raise ValueError
                        if primary_acc.primary_email:
                            iga_person.primary_email = primary_acc.primary_email
                        iga_person.feide_id = primary_acc.name + "@uio.no"
            except StopIteration:
                pass

        if {"sap_person_id", "fs_person_id", "greg_person_id"} & fields_to_include:
            # Both use externalids endpoint, and result is cached giving both with one
            # call
            sap_person_id = self.get_employee_nr(account.owner.id)
            if sap_person_id:
                iga_person.sap_person_id = sap_person_id
            fs_person_id = self.get_student_nr(account.owner.id)
            if fs_person_id:
                iga_person.fs_person_id = fs_person_id
            greg_person_id = self.get_greg_person_id(account.owner.id)
            if greg_person_id:
                iga_person.greg_person_id = greg_person_id

        if {"iga_person_id", "display_name"} & fields_to_include:
            person = self.client.get_person(account.owner.id)
            if person is None:
                raise ValueError
            iga_person.iga_person_id = str(person.id)
            disp_name = self.get_display_name(person, return_model=True)
            if disp_name:
                iga_person.display_name = disp_name

        if {"phone_numbers"} & fields_to_include:
            conts = self.client.get_person_contact(account.owner.id)
            if conts:
                iga_person.phone_numbers = self._contacts2phone_numbers(conts)

        return iga_person

    def get_igaperson_by_personid(
        self, person_id: Any, fields_to_include: Set[str]
    ) -> Optional[IgaPerson]:
        """Use IgaPerson.__fields__ if you want all fields"""
        if not fields_to_include:
            raise ValueError("fields_to_include can't be empty")

        iga_person = IgaPerson(iga_person_id=person_id)

        if {"accounts", "primary_email", "feide_id"} & fields_to_include:
            person_accounts = self.client.get_person_accounts(person_id)
            # map accounts to list of tuples with (account_type, username)
            accounts = list(
                map(
                    lambda x: IgaAccount(type="primary", username=x.name)
                    if x.primary
                    else IgaAccount(type="secondary", username=x.name),
                    person_accounts or (),
                )
            )
            iga_person.accounts = accounts
            if {"primary_email", "feide_id"} & fields_to_include:
                try:
                    prim_acc_info = next(
                        filter(lambda x: x.type == "primary", accounts)
                    )
                    primary_acc = self.client.get_account(prim_acc_info.username)
                    if primary_acc is None:
                        raise ValueError
                    if primary_acc.primary_email:
                        iga_person.primary_email = primary_acc.primary_email
                    iga_person.feide_id = primary_acc.name + "@uio.no"
                except StopIteration:
                    pass

        if {"sap_person_id", "fs_person_id", "greg_person_id"} & fields_to_include:
            # Both use externalids endpoint, and result is cached giving both with one
            # call
            sap_person_id = self.get_employee_nr(person_id)
            if sap_person_id:
                iga_person.sap_person_id = sap_person_id
            fs_person_id = self.get_student_nr(person_id)
            if fs_person_id:
                iga_person.fs_person_id = fs_person_id
            greg_person_id = self.get_greg_person_id(person_id)
            if greg_person_id:
                iga_person.greg_person_id = greg_person_id

        if {"display_name"} & fields_to_include:
            person = self.client.get_person(person_id)
            if person is None:
                raise ValueError
            disp_name = self.get_display_name(person, return_model=True)
            if disp_name:
                iga_person.display_name = disp_name

        if {"phone_numbers"} & fields_to_include:
            conts = self.client.get_person_contact(person_id)
            if conts:
                iga_person.phone_numbers = self._contacts2phone_numbers(conts)

        return iga_person

    @staticmethod
    def create_iga_group(group: Union[GroupListItem, Group]) -> IgaGroup:
        return IgaGroup(
            id=str(group.id),
            name=group.name,
            href=group.href,
            created_date=group.created_at,
            expire_date=group.expire_date,
            description=group.description,
            visibility=group.visibility,
        )

    def get_all_greg_persons(self) -> Iterable[IgaResult]:
        raise NotImplementedError
