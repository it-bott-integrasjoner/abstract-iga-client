from datetime import datetime
from typing import List, Optional, Tuple

import pydantic


class BaseModel(pydantic.BaseModel):
    class Config:
        # Perform validation on assignment to attributes
        validate_assignment = True
        extra = pydantic.Extra.forbid


class Affiliation(BaseModel):
    pass


class IgaAccount(BaseModel):
    type: str
    username: str
    email: Optional[str]


class PhoneNumber(BaseModel):
    type: str
    source: str
    value: str


class DisplayName(BaseModel):
    first: str
    last: str


class IgaGroup(BaseModel):
    id: str
    name: str
    description: Optional[str]
    created_date: datetime

    # specific to cerebrum may be empty for other iga
    href: Optional[str]
    expire_date: Optional[datetime]
    visibility: Optional[str]


class IgaResult(BaseModel):
    """
    Data class used to standardize iga results
    """

    iga_person_id: Optional[str]
    sap_person_id: Optional[str]
    fs_person_id: Optional[str]
    greg_person_id: Optional[str]
    username: Optional[str]
    email: Optional[str]
    display_name: Tuple[Optional[str], Optional[str]] = (None, None)
    affiliations: Optional[List[Affiliation]]
    org_import_id: Optional[str]
    roles: Optional[List[str]]


class IgaPerson(BaseModel):
    iga_person_id: Optional[str]
    sap_person_id: Optional[str]
    fs_person_id: Optional[str]
    greg_person_id: Optional[str]
    display_name: Optional[DisplayName]  # first, last
    primary_email: Optional[str]
    phone_numbers: Optional[List[PhoneNumber]]  # type, source, value
    accounts: Optional[List[IgaAccount]]
    feide_id: Optional[str]
