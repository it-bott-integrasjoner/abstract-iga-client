from typing import List, Optional, Set, Iterable, Tuple

import requests
from ntnu_tia_client import TiaClient
from ntnu_tia_client.models import Person, KeyTypeEnum, NameTypeEnum

from abstract_iga_client.config import NtnuTiaConfig
from abstract_iga_client.iga_abstract import IgaAbstract
from abstract_iga_client.iga_result import IgaResult, IgaPerson, IgaGroup


def get_display_name_tuple(person: Person) -> Tuple[Optional[str], Optional[str]]:
    if not person.name:
        return None, None
    first, last = None, None
    display_name = person.name.get(NameTypeEnum.displayname)
    if display_name:
        parts = display_name.rsplit(" ", maxsplit=1)
        if len(parts) == 2:
            first, last = parts
    if not (first and last):
        first, last = (
            person.name.get(NameTypeEnum.first_name),
            person.name.get(NameTypeEnum.last_name),
        )
    return first, last


class IgaCerebrumNtnu(IgaAbstract):
    SAP_ID_TYPE_EMPLOYEE = KeyTypeEnum.dfo_ansatt_id
    BAS_ID_TYPE_USERNAME = KeyTypeEnum.username
    IGA_ID_TYPE_EMPLOYEE = KeyTypeEnum.basid
    FS_ID_TYPE_LOPENR = KeyTypeEnum.fslopenr
    GREG_ID_TYPE_ID = KeyTypeEnum.greg_person_id

    def __init__(self, session: Optional[requests.Session], *, config: NtnuTiaConfig):
        super().__init__(session)
        self.client = TiaClient(
            url=config.url,
            use_sessions=self.session or False,
            headers=config.headers,
            rewrite_url=config.rewrite_url,
        )

    def get_person_by_username(self, username: str) -> Optional[IgaResult]:
        """
        :param username: Username in cerebrum
        :return: IgaResult object
        """
        person = self.client.get_bas_person(self.BAS_ID_TYPE_USERNAME, username)
        if person is None:
            self.logger.warning("Error in get_person from iga for {}".format(username))
            return None
        return self.transform_result(person)

    def get_person_by_id(self, person_id: int) -> Optional[IgaResult]:
        raise NotImplementedError

    def get_person_by_employee_nr(self, employee_nr: str) -> Optional[IgaResult]:
        person = self.client.get_bas_person(self.SAP_ID_TYPE_EMPLOYEE, employee_nr)
        if person is None:
            self.logger.warning(
                "Error in get_person from iga for {}".format(employee_nr)
            )
            return None
        return self.transform_result(person)

    def get_person_by_fs_id(self, fs_id: str) -> Optional[IgaResult]:
        person = self.client.get_bas_person(self.FS_ID_TYPE_LOPENR, fs_id)
        if person is None:
            self.logger.warning(f"Error getting data from iga for fs_id {fs_id}")
            return None
        return self.transform_result(person)

    def get_person_by_greg_person_id(self, value: int) -> Optional[IgaResult]:
        person = self.client.get_bas_person(self.GREG_ID_TYPE_ID, value.__str__())
        if person is None:
            self.logger.warning("Error in get_person from iga for {}".format(value))
            return None
        return self.transform_result(person)

    def get_groups(self, username: str) -> Optional[List[IgaGroup]]:
        raise NotImplementedError

    def get_group_members(self, group_name: str) -> List[str]:
        raise NotImplementedError

    def get_all_persons(self) -> Optional[List[IgaResult]]:
        raise NotImplementedError

    def get_all_employees(self) -> Optional[List[IgaResult]]:
        raise NotImplementedError

    def transform_result(self, person: Person) -> IgaResult:
        """
        Method to transform iga result to standardized iga_result object
        :param person: Result from iga client query
        :param affiliations: affiliations for person
        :return: IgaResult object
        """
        org_import_id = (
            ""  # TODO: Find correct org_import_id when affiliations are available
        )
        # Transform into standard result object
        return IgaResult(
            sap_person_id=person.keys.get(self.SAP_ID_TYPE_EMPLOYEE),
            fs_person_id=person.keys.get(self.FS_ID_TYPE_LOPENR),
            iga_person_id=person.keys.get(self.IGA_ID_TYPE_EMPLOYEE),
            greg_person_id=person.keys.get(self.GREG_ID_TYPE_ID),
            username=person.keys.get(self.BAS_ID_TYPE_USERNAME),
            email=person.keys.get(KeyTypeEnum.email),
            display_name=get_display_name_tuple(person),
            affiliations=[],
            org_import_id=org_import_id,
        )

    def get_person_consents(self, person_id: int) -> Optional[Iterable[object]]:
        raise NotImplementedError

    def get_igaperson_by_username(
        self, username: str, fields_to_include: Set[str]
    ) -> Optional[IgaPerson]:
        raise NotImplementedError

    def get_igaperson_by_personid(
        self, person_id: str, fields_to_include: Set[str]
    ) -> Optional[IgaPerson]:
        raise NotImplementedError

    def get_all_greg_persons(self) -> Iterable[IgaResult]:
        raise NotImplementedError
