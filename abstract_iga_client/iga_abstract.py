from __future__ import annotations

import logging
from abc import ABC, abstractmethod
from typing import Union, List, Optional, Iterable, Set, Any

import requests

from abstract_iga_client.config import ClientConfig
from abstract_iga_client.iga_result import IgaResult, IgaPerson, IgaGroup


class IgaAbstract(ABC):
    """
    This is the abstract class that defines the methods the implementation classes should have

    How to use dynamically in client code:

    import importlib

    from cristin_ms.config import get_config

    config = get_config()

    iga_module = importlib.import_module(config.cristin_ms.get('iga')['module_name'])
    class_ = getattr(iga_module, config.cristin_ms.get('iga')['implementation_class'])
    client = class_()

    result = client.get_person_by_id('eje003')


    """

    def __init__(self, session: Optional[requests.Session]) -> None:
        self.session = session
        self.logger = logging.getLogger()

    @abstractmethod
    def get_person_by_id(self, person_id: Any) -> Optional[IgaResult]:
        """
        Get a person by id from iga

        :param person_id: The id needed to find a person in the iga client
        :return: IgaResult object
        """
        pass

    @abstractmethod
    def get_person_by_username(self, username: Any) -> Optional[IgaResult]:
        """
        Get a person by username from iga
        :param username: The username needed to find a person in the iga client
        :return: IgaResult object
        """
        pass

    @abstractmethod
    def get_person_by_employee_nr(self, employee_nr: Any) -> Optional[IgaResult]:
        pass

    @abstractmethod
    def get_person_by_fs_id(self, fs_id: Any) -> Optional[IgaResult]:
        pass

    @abstractmethod
    def get_person_by_greg_person_id(self, value: int) -> Optional[IgaResult]:
        pass

    @abstractmethod
    def get_groups(self, username: Any) -> Optional[List[IgaGroup]]:
        """Get the groups that belongs to the user"""
        pass

    @abstractmethod
    def get_group_members(self, group_name: str) -> List[str]:
        """Get the usernames of accounts that belong in the group"""
        pass

    @abstractmethod
    def get_all_persons(self) -> Union[List[IgaResult], None]:
        """
        Get all persons from iga
        :return: List of IgaResult objects
        """
        pass

    @abstractmethod
    def get_all_employees(self) -> Union[List[IgaResult], None]:
        """
        Get all persons with an employee id from iga
        """

    @abstractmethod
    def get_all_greg_persons(self) -> Iterable[IgaResult]:
        """
        Get all persons with a guest id from iga
        """

    @abstractmethod
    def get_person_consents(self, person_id: Any) -> Optional[Iterable[object]]:
        """
        Get a person's consents from iga

        Only implemented for UiO Cerebrum. If more clients implement a similar feature
        we should create a general Consent model
        """

    @abstractmethod
    def get_igaperson_by_username(
        self, username: Any, fields_to_include: Set[str]
    ) -> Optional[IgaPerson]:
        """New method for getting IgaPerson

        Uses a new data model where an IgaPerson can have multiple accounts
        and attempts to
        """

    @abstractmethod
    def get_igaperson_by_personid(
        self, person_id: Any, fields_to_include: Set[str]
    ) -> Optional[IgaPerson]:
        """New method for getting IgaPerson

        Uses a new data model where an IgaPerson can have multiple accounts
        and attempts to
        """

    @staticmethod
    def get_iga_client(
        config: ClientConfig,
        session: Optional[requests.Session] = None,
    ) -> IgaAbstract:
        from abstract_iga_client.config import (
            UibScimRiConfig,
            UioCerebrumConfig,
            UitScimRiConfig,
            NtnuTiaConfig,
        )

        if isinstance(config, UioCerebrumConfig):
            from .iga_cerebrum_uio import IgaCerebrumUio

            return IgaCerebrumUio(session=session, config=config)
        if isinstance(config, NtnuTiaConfig):
            from .iga_cerebrum_ntnu import IgaCerebrumNtnu

            return IgaCerebrumNtnu(session=session, config=config)
        if isinstance(config, UibScimRiConfig):
            from .iga_scim_ri_uib import IgaClient

            return IgaClient(session=session, config=config)
        if isinstance(config, UitScimRiConfig):
            from .iga_scim_ri_uit import IgaClientUit

            return IgaClientUit(session=session, config=config)
        if not isinstance(config, ClientConfig):
            raise TypeError
        raise ValueError
