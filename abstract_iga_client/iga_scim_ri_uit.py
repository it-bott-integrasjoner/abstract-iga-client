from __future__ import annotations

import logging
from typing import (
    Set,
    Optional,
    Iterable,
    Union,
    List,
    Callable,
    TypeVar,
    Sequence,
    TYPE_CHECKING,
    Any,
)

import requests
import scim2_client as scim
from pydantic import Field, ValidationError, conint
from scim2_client import (
    BaseModel,
    Client,
    ItemRequest,
    QueryRequest,
    EmailType,
    Request,
    QueryParameters,
    AttributesParameter,
    Filter,
    ScimError,
    ListResponse,
    Equal,
    Compare,
    StartsWith,
    Present,
    AttrExpr,
)

from abstract_iga_client.config import UitScimRiConfig
from abstract_iga_client.iga_abstract import IgaAbstract
from abstract_iga_client.iga_result import (
    IgaPerson,
    IgaResult,
    IgaGroup,
    DisplayName,
    PhoneNumber,
    IgaAccount,
)

logger = logging.getLogger(__name__)

T = TypeVar("T")

if TYPE_CHECKING:
    ItemsPerPageT = int
else:
    ItemsPerPageT = conint(ge=10)


class NoEduScimUser(BaseModel):
    account_type: Optional[str] = Field(alias="accountType")
    employee_number: Optional[str] = Field(alias="employeeNumber")
    fs_person_number: Optional[str] = Field(alias="fsPersonNumber")
    greg_person_number: Optional[str] = Field(alias="gregPersonNumber")
    edu_person_principal_name: Optional[str] = Field(alias="eduPersonPrincipalName")


NO_EDU_SCIM_USER = "no:edu:scim:user"

USER_NAME_ATTR = "userName"
ACCOUNT_TYPE_ATTR = f"{NO_EDU_SCIM_USER}:accountType"
EMPLOYEE_NUMBER_ATTR = f"{NO_EDU_SCIM_USER}:employeeNumber"
FS_PERSON_NUMBER_ATTR = f"{NO_EDU_SCIM_USER}:fsPersonNumber"
EDU_PERSON_PRINCIPAL_NAME_ATTR = f"{NO_EDU_SCIM_USER}:eduPersonPrincipalName"
GREG_PERSON_NUMBER_ATTR = f"{NO_EDU_SCIM_USER}:gregPersonNumber"

USER_ATTRS = AttributesParameter(
    attributes=(
        "id",
        "name",
        "active",
        "emails",
        "phoneNumbers",
        "roles",
        "meta.resourceType",
        ACCOUNT_TYPE_ATTR,
        USER_NAME_ATTR,
        EMPLOYEE_NUMBER_ATTR,
        FS_PERSON_NUMBER_ATTR,
        EDU_PERSON_PRINCIPAL_NAME_ATTR,
        GREG_PERSON_NUMBER_ATTR,
    )
)
IS_ACTIVE = Equal("active", True)
IS_EMPLOYEE = Present(EMPLOYEE_NUMBER_ATTR)
IS_GUEST = Present(GREG_PERSON_NUMBER_ATTR)


class User(scim.User):
    no_edu_scim_user: NoEduScimUser = Field(alias=NO_EDU_SCIM_USER)


def user_query(
    *,
    count: Optional[int] = None,
    start_index: Optional[int] = None,
    attr_expr: Optional[AttrExpr] = None,
    can_use_complex_filters: bool,
) -> QueryRequest[scim.ListResponse[User]]:
    class UserListResponse(scim.ListResponse[User]):
        resources: Sequence[User] = Field((), alias="Resources")

    if can_use_complex_filters:
        filter_ = (
            Filter(IS_ACTIVE)
            if attr_expr is None
            else Filter(IS_ACTIVE).group().and_(Filter(attr_expr).group())
        )
    else:
        filter_ = Filter(IS_ACTIVE) if attr_expr is None else Filter(attr_expr)
    return QueryRequest(
        request_type=Request.Type.USER,
        result_type=UserListResponse,
        parameters=QueryParameters(
            attributes=USER_ATTRS,
            count=count,
            start_index=start_index,
            filter=filter_,
        ),
    )


class IgaClientUit(IgaAbstract):
    def __init__(
        self, session: Optional[requests.Session], *, config: UitScimRiConfig
    ) -> None:
        super().__init__(session)
        self.config = config
        self.client = Client(session=self.session, config=config)

    def _get_user_by_id(self, user_id: str) -> Optional[User]:
        try:
            user: User = self.client(
                ItemRequest(
                    request_type=Request.Type.USER,
                    result_type=User,
                    id=user_id,
                    attributes=USER_ATTRS,
                )
            ).payload
            if user and user.active:
                return user
            return None
        except ScimError as exc:
            if exc.response.status == "404":
                return None
            raise

    def get_person_by_id(self, person_id: str) -> Optional[IgaResult]:
        assert person_id
        return _to_iga_result(self._get_user_by_id(person_id))

    def get_person_by_username(self, username: str) -> Optional[IgaResult]:
        assert username
        compare: Compare
        if "@" in username:
            compare = Equal(USER_NAME_ATTR, username)
        else:
            # filter='userName eq "XXXX"' doesn't return results for
            # unqualified usernames anymore.  The unqualified form is
            # supported for the userName=XXXX form, or one can use
            # filter='userName sw "XXXX@" instead
            compare = StartsWith(USER_NAME_ATTR, f"{username}@")
        return self._transform_single_user_query_request(
            user_query(
                attr_expr=compare,
                can_use_complex_filters=self.config.can_use_complex_filters,
            ),
            _to_iga_result,
        )

    def get_person_by_employee_nr(self, employee_nr: str) -> Optional[IgaResult]:
        assert employee_nr
        return self._transform_single_user_query_request(
            user_query(
                attr_expr=Equal(EMPLOYEE_NUMBER_ATTR, str(employee_nr)),
                can_use_complex_filters=self.config.can_use_complex_filters,
            ),
            _to_iga_result,
        )

    def get_person_by_fs_id(self, fs_id: str) -> Optional[IgaResult]:
        assert fs_id
        return self._transform_single_user_query_request(
            user_query(
                attr_expr=Equal(FS_PERSON_NUMBER_ATTR, str(fs_id)),
                can_use_complex_filters=self.config.can_use_complex_filters,
            ),
            _to_iga_result,
        )

    def get_person_by_greg_person_id(self, value: int) -> Optional[IgaResult]:
        assert value
        return self._transform_single_user_query_request(
            user_query(
                attr_expr=Equal(GREG_PERSON_NUMBER_ATTR, str(value)),
                can_use_complex_filters=self.config.can_use_complex_filters,
            ),
            _to_iga_result,
        )

    def get_groups(self, username: str) -> Optional[List[IgaGroup]]:
        raise NotImplementedError

    def get_group_members(self, group_name: str) -> List[str]:
        raise NotImplementedError

    def get_all_persons(self) -> Union[List[IgaResult], None]:
        raise NotImplementedError

    def get_all_employees(self) -> List[IgaResult]:
        return list(self.get_all_by_attr_expr(IS_EMPLOYEE))

    def get_all_greg_persons(self) -> Iterable[IgaResult]:
        return self.get_all_by_attr_expr(IS_GUEST)

    def get_all_by_attr_expr(self, attr_expr: AttrExpr) -> Iterable[IgaResult]:
        return filter(
            None,
            (
                _to_iga_result(x)
                for x in self.client.iter_resources(
                    user_query(
                        count=self.config.items_per_page,
                        can_use_complex_filters=self.config.can_use_complex_filters,
                        attr_expr=attr_expr,
                    )
                )
            ),
        )

    def get_person_consents(self, person_id: Any) -> Optional[Iterable[object]]:
        raise NotImplementedError

    def get_igaperson_by_username(
        self, username: str, fields_to_include: Set[str]
    ) -> Optional[IgaPerson]:
        assert username
        assert fields_to_include
        iga_person = self._transform_single_user_query_request(
            user_query(
                attr_expr=Equal(USER_NAME_ATTR, username),
                can_use_complex_filters=self.config.can_use_complex_filters,
            ),
            _to_iga_person,
        )
        return iga_person

    def get_igaperson_by_personid(
        self, person_id: str, fields_to_include: Set[str]
    ) -> Optional[IgaPerson]:
        assert person_id
        assert fields_to_include
        user = self._get_user_by_id(person_id)
        return _to_iga_person(user)

    def _transform_single_user_query_request(
        self,
        request: QueryRequest[ListResponse[User]],
        transform: Callable[[User | None], T],
    ) -> Optional[T]:
        active_user: User | None = None
        for user in self.client.iter_resources(request):
            if user.active:
                if active_user is not None:
                    # FIXME: maybe get_person_by_* should return an iterable instead of a single object?
                    logger.error(
                        "Expected only 1 active account for request %s", request
                    )
                    return None
                active_user = user
        return transform(active_user)


def _email_work(user: User) -> Optional[str]:
    return next((x.value for x in user.emails or () if x.type == EmailType.WORK), None)


def _to_iga_result(user: Optional[User]) -> Optional[IgaResult]:
    if user is None:
        return None
    if not user.active:
        logger.info("User %s is not active, discarding", user.id)
        return None
    scim_user = user.no_edu_scim_user
    try:
        if user.name:
            display_name = (user.name.given_name, user.name.family_name)
        else:
            display_name = (None, None)
        roles = (
            [role.value for role in user.roles if role.value is not None]
            if user.roles
            else []
        )

        return IgaResult(
            iga_person_id=user.id,
            sap_person_id=scim_user.employee_number,
            fs_person_id=scim_user.fs_person_number,
            greg_person_id=scim_user.greg_person_number,
            username=user.user_name,
            email=_email_work(user),
            display_name=display_name,
            affiliations=[],
            org_import_id="",
            roles=roles,
        )
    except ValidationError as exc:
        logger.warning("Unable to construct a valid IgaResult: %s", exc)
        return None


def _to_iga_person(user: Optional[User]) -> Optional[IgaPerson]:
    if user is None:
        return None
    if not user.active:
        logger.info("User %s is not active, discarding", user.id)
        return None
    try:
        email = _email_work(user)
        return IgaPerson(
            iga_person_id=user.id,
            sap_person_id=user.no_edu_scim_user.employee_number,
            fs_person_id=user.no_edu_scim_user.fs_person_number,
            greg_person_id=user.no_edu_scim_user.greg_person_number,
            display_name=(
                DisplayName(first=user.name.given_name, last=user.name.family_name)
                if user.name and user.name.given_name and user.name.family_name
                else None
            ),
            primary_email=email,
            phone_numbers=[
                PhoneNumber(type=x.type, value=x.value, source="")
                for x in user.phone_numbers or ()
                if x.type and x.value
            ],
            accounts=[
                IgaAccount(
                    type=user.no_edu_scim_user.account_type or "",
                    username=user.user_name,  # type: ignore[arg-type]
                    email=email,
                )
            ],
            feide_id=user.no_edu_scim_user.edu_person_principal_name,
        )
    except ValidationError as exc:
        logger.warning("Unable to construct a valid IgaPerson: %s", exc)
        return None
