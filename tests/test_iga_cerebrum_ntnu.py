import pytest
import requests
from ntnu_tia_client.models import Person, NameTypeEnum

from abstract_iga_client.config import parse_client_config
from abstract_iga_client.iga_cerebrum_ntnu import (
    IgaCerebrumNtnu,
    IgaAbstract,
    get_display_name_tuple,
)
from abstract_iga_client.iga_result import IgaResult


@pytest.fixture
def ola_normann_iga_result():
    return IgaResult(
        iga_person_id="998877",
        sap_person_id="12345678",
        fs_person_id="1234567890",
        greg_person_id="123",
        username="olanormann99",
        email="ola.g.normann@example.ntnu.no",
        display_name=("Ola", "Normann"),
        affiliations=[],
        org_import_id="",
    )


@pytest.mark.parametrize(
    argnames=[
        "session",
    ],
    argvalues=[
        [None],
        [requests.Session()],
    ],
)
def test_init_with_session(session, iga_cerebrum_ntnu_init_kwargs):
    client = IgaAbstract.get_iga_client(
        session=session, config=parse_client_config(**iga_cerebrum_ntnu_init_kwargs)
    )

    assert isinstance(client, IgaCerebrumNtnu)
    if session is None:
        assert client.client.session is requests
    else:
        assert client.client.session is session


def test_get_person_by_username(
    iga_cerebrum_ntnu: IgaCerebrumNtnu, ola_normann_iga_result
):
    iga_result = iga_cerebrum_ntnu.get_person_by_username("olanormann99")

    assert iga_result == ola_normann_iga_result


def test_get_person_by_username_resturn_none_when_missing(
    iga_cerebrum_ntnu: IgaCerebrumNtnu,
):
    iga_result = iga_cerebrum_ntnu.get_person_by_username("nosuchuser")
    assert iga_result is None


def test_get_person_by_employee_nr(
    iga_cerebrum_ntnu: IgaCerebrumNtnu, ola_normann_iga_result
):
    iga_result = iga_cerebrum_ntnu.get_person_by_employee_nr("12345678")

    assert iga_result == ola_normann_iga_result


def test_get_person_by_employee_nr_return_none_when_missing(
    iga_cerebrum_ntnu: IgaCerebrumNtnu,
):
    iga_result = iga_cerebrum_ntnu.get_person_by_employee_nr("not_existing")
    assert iga_result is None


def test_get_person_by_fs_id(
    iga_cerebrum_ntnu: IgaCerebrumNtnu, ola_normann_iga_result
):
    iga_result = iga_cerebrum_ntnu.get_person_by_fs_id("1234567890")

    assert iga_result == ola_normann_iga_result


def test_get_person_by_fs_id_return_none_when_missing(
    iga_cerebrum_ntnu: IgaCerebrumNtnu,
):
    iga_result = iga_cerebrum_ntnu.get_person_by_fs_id("not_existing")
    assert iga_result is None


def test_get_person_by_greg_person_id(
    iga_cerebrum_ntnu: IgaCerebrumNtnu, ola_normann_iga_result
):
    iga_result = iga_cerebrum_ntnu.get_person_by_greg_person_id(123)

    assert iga_result == ola_normann_iga_result


def test_get_person_by_greg_person_id_return_none_when_missing(
    iga_cerebrum_ntnu: IgaCerebrumNtnu,
):
    iga_result = iga_cerebrum_ntnu.get_person_by_greg_person_id(1)
    assert iga_result is None


def test_get_display_name_tuple_no_name():
    person = Person(lastUpdated=1, keys={})
    display_name = get_display_name_tuple(person)

    assert display_name == (None, None)


def test_get_display_name_tuple_single_display_name_no_fallback():
    person = Person(
        lastUpdated=1,
        keys={},
        name={NameTypeEnum.displayname: "Per"},
    )
    display_name = get_display_name_tuple(person)

    assert display_name == (None, None)


def test_get_display_name_tuple_with_complete_display_name():
    person = Person(
        lastUpdated=1,
        keys={},
        name={
            NameTypeEnum.displayname: "Per Pedersen",
            NameTypeEnum.first_name: "Atle",
            NameTypeEnum.last_name: "Antonsen",
        },
    )
    display_name = get_display_name_tuple(person)

    assert display_name == ("Per", "Pedersen")


@pytest.mark.parametrize(
    ("first_name", "last_name", "expected"),
    (
        ("Per", "Pedersen", ("Per", "Pedersen")),
        (None, "Pedersen", (None, "Pedersen")),
        ("Per", None, ("Per", None)),
    ),
)
def test_get_display_name_tuple_single_display_name_with_fallback(
    first_name, last_name, expected
):
    name = {NameTypeEnum.displayname: "Atle"}
    if first_name:
        name[NameTypeEnum.first_name] = first_name
    if last_name:
        name[NameTypeEnum.last_name] = last_name
    person = Person(
        lastUpdated=1,
        keys={},
        name=name,  # type: ignore[arg-type]
    )
    display_name = get_display_name_tuple(person)

    assert display_name == expected
