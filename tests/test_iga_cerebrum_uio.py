from types import SimpleNamespace

import pytest
import requests
from cerebrum_client.models import Person

from abstract_iga_client.config import parse_client_config, UioCerebrumConfig
from abstract_iga_client.iga_abstract import IgaAbstract
from abstract_iga_client.iga_cerebrum_uio import IgaCerebrumUio
from abstract_iga_client.iga_result import IgaPerson, Affiliation, IgaResult


@pytest.fixture
def expected_veralf2():
    return IgaResult(
        iga_person_id="308",
        sap_person_id="10137041",
        fs_person_id="475905",
        greg_person_id="1024",
        username="veralf2",
        email="test.email@uio.no",
        display_name=("Alfred", "vergaard"),
        affiliations=[Affiliation(), Affiliation()],
        org_import_id="221030",
    )


@pytest.mark.parametrize(
    argnames=[
        "session",
    ],
    argvalues=[
        [None],
        [requests.Session()],
    ],
)
def test_init_with_session(session, iga_cerebrum_uio_init_kwargs):
    client = IgaAbstract.get_iga_client(
        session=session, config=parse_client_config(**iga_cerebrum_uio_init_kwargs)
    )

    assert isinstance(client, IgaCerebrumUio)
    if session is None:
        assert client.client.session is requests  # type: ignore
    else:
        assert client.client.session is session


def test_get_person_by_id(
    iga_cerebrum_uio: IgaCerebrumUio, account_veralf2, expected_veralf2
):
    iga_result = iga_cerebrum_uio.get_person_by_id(account_veralf2["owner"]["id"])

    assert iga_result == expected_veralf2


def test_get_person_by_id_return_none_when_missing(iga_cerebrum_uio: IgaCerebrumUio):
    iga_result = iga_cerebrum_uio.get_person_by_id(-1)
    assert iga_result is None


def test_get_person_by_username(
    iga_cerebrum_uio: IgaCerebrumUio, account_veralf2, expected_veralf2
):
    iga_result = iga_cerebrum_uio.get_person_by_username(account_veralf2["name"])

    assert iga_result == expected_veralf2


def test_get_person_by_username_return_none_when_missing(
    iga_cerebrum_uio: IgaCerebrumUio,
):
    iga_result = iga_cerebrum_uio.get_person_by_username("account_that_do_not_exist")
    assert iga_result is None


def test_get_person_by_employee_nr(iga_cerebrum_uio: IgaCerebrumUio, expected_veralf2):
    employee_nr = "010137041"
    iga_result = iga_cerebrum_uio.get_person_by_employee_nr(employee_nr)

    assert iga_result == expected_veralf2


def test_get_person_by_employee_nr_leading_zeros(
    mock_api, external_id_search_result_10137041
):
    iga_cerebrum_uio = IgaCerebrumUio(
        config=UioCerebrumConfig(iga_impl="cerebrum@uio", url="http://example.com"),
        session=None,
    )
    employee_nr = "010137041"
    mock_api.get(
        "http://example.com/v1/search/persons/external-ids?source_system=DFO_SAP&id_type=dfoPersonId&external_id=10137041",
        json=external_id_search_result_10137041,
    )
    # Return 404 to return None early
    mock_api.get("http://example.com/v1/persons/308", status_code=404)

    iga_cerebrum_uio.get_person_by_employee_nr(employee_nr)

    assert employee_nr[0] == "0"
    # Make sure leading zeros was stripped
    assert mock_api.request_history[0].qs["external_id"] == [employee_nr.lstrip("0")]


def test_get_person_by_employee_nr_return_none_when_missing(
    iga_cerebrum_uio: IgaCerebrumUio,
):
    employee_nr = "not_existing"
    iga_result = iga_cerebrum_uio.get_person_by_employee_nr(employee_nr)
    assert iga_result is None


def test_get_groups_non_existing_user(iga_cerebrum_uio: IgaCerebrumUio):
    groups = iga_cerebrum_uio.get_groups("no-existing")
    assert groups is None


def test_get_groups_alice(iga_cerebrum_uio: IgaCerebrumUio, groups_alice):
    groups = iga_cerebrum_uio.get_groups("alice")

    # noinspection PyTypeChecker
    assert groups == [
        iga_cerebrum_uio.create_iga_group(SimpleNamespace(**x))  # type: ignore[arg-type]
        for x in groups_alice["groups"]
    ]


def test_get_display_name_return_none_when_name_not_found(
    iga_cerebrum_uio: IgaCerebrumUio, person_308
):
    person = iga_cerebrum_uio.client.get_person(person_308["id"])
    assert isinstance(person, Person)
    person.id = -999
    person.names = []
    display_name = iga_cerebrum_uio.get_display_name(person)
    assert display_name == (None, None)


def test_get_display_name_return_none_when_person_not_found(
    iga_cerebrum_uio: IgaCerebrumUio,
):
    display_name = iga_cerebrum_uio.get_display_name(None)
    assert display_name == (None, None)


def test_get_all_persons(iga_cerebrum_uio: IgaCerebrumUio):
    with pytest.raises(NotImplementedError):
        iga_cerebrum_uio.get_all_persons()


def test_get_primary_account_return_none_for_unknown_person(
    iga_cerebrum_uio: IgaCerebrumUio,
):
    account = iga_cerebrum_uio.get_primary_account(-999)
    assert account is None


def test_get_igaperson_by_username(
    requests_mock,
    account_alice,
    person_alice_accounts,
    person_alice_externalids,
    person_alice,
    person_alice_contacts,
    igaperson_alice,
):
    expected = IgaPerson(**igaperson_alice)

    client = IgaAbstract.get_iga_client(
        config=parse_client_config(
            iga_impl="cerebrum@uio", url="https://example.com/cerebrum/"
        )
    )
    requests_mock.get(
        "https://example.com/cerebrum/v1/accounts/alice", json=account_alice
    )
    requests_mock.get(
        "https://example.com/cerebrum/v1/persons/0/accounts", json=person_alice_accounts
    )
    requests_mock.get(
        "https://example.com/cerebrum/v1/persons/0/external-ids",
        json=person_alice_externalids,
    )
    requests_mock.get("https://example.com/cerebrum/v1/persons/0", json=person_alice)
    requests_mock.get(
        "https://example.com/cerebrum/v1/persons/0/contacts", json=person_alice_contacts
    )
    actual = client.get_igaperson_by_username("alice", set(IgaPerson.__fields__))

    assert actual == expected


def test_get_igaperson_by_username_incomplete(
    requests_mock,
    account_alice,
    person_alice_accounts,
    person_alice_externalids,
    person_alice_contacts,
    igaperson_alice: dict,
):
    """
    Check that only the fields should be set are set when not asking for all fields
    """
    del igaperson_alice["iga_person_id"]
    del igaperson_alice["display_name"]
    expected = IgaPerson(**igaperson_alice)

    client = IgaAbstract.get_iga_client(
        config=parse_client_config(
            iga_impl="cerebrum@uio", url="https://example.com/cerebrum/"
        )
    )
    requests_mock.get(
        "https://example.com/cerebrum/v1/accounts/alice", json=account_alice
    )
    requests_mock.get(
        "https://example.com/cerebrum/v1/persons/0/accounts", json=person_alice_accounts
    )
    requests_mock.get(
        "https://example.com/cerebrum/v1/persons/0/external-ids",
        json=person_alice_externalids,
    )
    requests_mock.get(
        "https://example.com/cerebrum/v1/persons/0/contacts", json=person_alice_contacts
    )
    actual = client.get_igaperson_by_username(
        "alice",
        {"primary_email", "feide_id", "phone_numbers", "sap_person_id", "accounts"},
    )

    assert actual == expected


def test_get_igaperson_non_primary(
    requests_mock,
    igaperson_alice,
    account_alice_drift,
    person_alice_accounts,
    account_alice,
    person_alice_externalids,
    person_alice,
    person_alice_contacts,
):
    """
    Check that we find the person even if the account is not the primary account
    """
    expected = IgaPerson(**igaperson_alice)

    client = IgaAbstract.get_iga_client(
        config=parse_client_config(
            iga_impl="cerebrum@uio", url="https://example.com/cerebrum/"
        )
    )
    requests_mock.get(
        "https://example.com/cerebrum/v1/accounts/alice-drift", json=account_alice_drift
    )
    requests_mock.get(
        "https://example.com/cerebrum/v1/persons/0/accounts", json=person_alice_accounts
    )
    requests_mock.get(
        "https://example.com/cerebrum/v1/accounts/alice", json=account_alice
    )
    requests_mock.get(
        "https://example.com/cerebrum/v1/persons/0/external-ids",
        json=person_alice_externalids,
    )
    requests_mock.get("https://example.com/cerebrum/v1/persons/0", json=person_alice)
    requests_mock.get(
        "https://example.com/cerebrum/v1/persons/0/contacts", json=person_alice_contacts
    )

    actual = client.get_igaperson_by_username("alice-drift", set(IgaPerson.__fields__))

    assert actual == expected


def test_get_igaperson_by_username_no_primary_account(
    requests_mock,
    account_alice,
    person_alice_accounts_no_primary,
    person_alice_externalids,
    person_alice,
    person_alice_contacts,
    igaperson_alice_no_primary_accounts,
):
    client = IgaAbstract.get_iga_client(
        config=parse_client_config(
            iga_impl="cerebrum@uio", url="https://example.com/cerebrum/"
        )
    )
    requests_mock.get(
        "https://example.com/cerebrum/v1/accounts/alice", json=account_alice
    )
    requests_mock.get(
        "https://example.com/cerebrum/v1/persons/0/accounts",
        json=person_alice_accounts_no_primary,
    )
    requests_mock.get(
        "https://example.com/cerebrum/v1/persons/0/external-ids",
        json=person_alice_externalids,
    )
    requests_mock.get("https://example.com/cerebrum/v1/persons/0", json=person_alice)
    requests_mock.get(
        "https://example.com/cerebrum/v1/persons/0/contacts", json=person_alice_contacts
    )
    actual = client.get_igaperson_by_username("alice", set(IgaPerson.__fields__))

    assert actual == igaperson_alice_no_primary_accounts


def test_get_igaperson_by_personid(
    requests_mock,
    igaperson_alice,
    person_alice_accounts,
    account_alice,
    person_alice_externalids,
    person_alice,
    person_alice_contacts,
):
    expected = IgaPerson(**igaperson_alice)

    client = IgaAbstract.get_iga_client(
        parse_client_config(
            iga_impl="cerebrum@uio", url="https://example.com/cerebrum/"
        )
    )
    requests_mock.get("https://example.com/cerebrum/v1/persons/0", json=person_alice)
    requests_mock.get(
        "https://example.com/cerebrum/v1/persons/0/accounts", json=person_alice_accounts
    )
    requests_mock.get(
        "https://example.com/cerebrum/v1/accounts/alice", json=account_alice
    )
    requests_mock.get(
        "https://example.com/cerebrum/v1/persons/0/external-ids",
        json=person_alice_externalids,
    )
    requests_mock.get(
        "https://example.com/cerebrum/v1/persons/0/contacts", json=person_alice_contacts
    )
    actual = client.get_igaperson_by_personid("0", set(IgaPerson.__fields__))

    assert actual == expected


def test_get_igaperson_by_personid_no_primary_accounts(
    requests_mock,
    igaperson_alice_no_primary_accounts,
    person_alice,
    person_alice_accounts_no_primary,
    account_alice,
    person_alice_externalids,
    person_alice_contacts,
):
    client = IgaAbstract.get_iga_client(
        config=parse_client_config(
            iga_impl="cerebrum@uio", url="https://example.com/cerebrum/"
        )
    )
    requests_mock.get("https://example.com/cerebrum/v1/persons/0", json=person_alice)
    requests_mock.get(
        "https://example.com/cerebrum/v1/persons/0/accounts",
        json=person_alice_accounts_no_primary,
    )
    requests_mock.get(
        "https://example.com/cerebrum/v1/accounts/alice", json=account_alice
    )
    requests_mock.get(
        "https://example.com/cerebrum/v1/persons/0/external-ids",
        json=person_alice_externalids,
    )
    requests_mock.get(
        "https://example.com/cerebrum/v1/persons/0/contacts", json=person_alice_contacts
    )
    actual = client.get_igaperson_by_personid("0", set(IgaPerson.__fields__))

    assert actual == igaperson_alice_no_primary_accounts


def test_get_person_by_greg_person_id(
    iga_cerebrum_uio: IgaCerebrumUio, expected_veralf2
):
    iga_result = iga_cerebrum_uio.get_person_by_greg_person_id(1024)

    assert iga_result == expected_veralf2


def test_get_person_by_greg_person_id_return_none_when_missing(
    iga_cerebrum_uio: IgaCerebrumUio,
):
    iga_result = iga_cerebrum_uio.get_person_by_greg_person_id(-1)
    assert iga_result is None


def test_get_group_members(
    iga_cerebrum_uio: IgaCerebrumUio,
):
    members = iga_cerebrum_uio.get_group_members("testgroup")

    assert members == ["impulsik"]


def test_get_group_members_no_members(
    iga_cerebrum_uio: IgaCerebrumUio,
):
    members = iga_cerebrum_uio.get_group_members("unknown$%^")

    assert members == []
