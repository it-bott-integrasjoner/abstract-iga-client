import json
import os
import re
from typing import Optional, Iterator, Any

import pytest
import requests_mock
from cerebrum_client.models import (
    OwnerRef,
    Account,
    Person,
    PersonExternalId,
    Affiliation,
    PersonExternalIdSearchResult,
    AccountRef,
    OU,
    GroupListItem,
    GroupMember,
)
from ntnu_tia_client.models import KeyTypeEnum
from pydantic import SecretStr

import abstract_iga_client.iga_cerebrum_ntnu
import abstract_iga_client.iga_cerebrum_uio
import abstract_iga_client.iga_scim_ri_uib
import abstract_iga_client.iga_scim_ri_uit
from abstract_iga_client.config import parse_client_config

from abstract_iga_client.iga_abstract import IgaAbstract
from abstract_iga_client.iga_result import IgaResult, IgaPerson


def load_json_file(name):
    here = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
    with open(os.path.join(here, "fixtures", name)) as f:
        data = json.load(f)
    return data


@pytest.fixture
def account_veralf2():
    return load_json_file("cerebrum_uio/account_veralf2.json")


@pytest.fixture
def person_308():
    return load_json_file("cerebrum_uio/person_308.json")


@pytest.fixture
def person_accounts_308():
    return load_json_file("cerebrum_uio/person_accounts_308.json")


@pytest.fixture
def external_ids_308():
    return load_json_file("cerebrum_uio/external_ids_308.json")


@pytest.fixture
def external_id_search_result_10137041():
    return load_json_file("cerebrum_uio/external_id_search_result_10137041.json")


@pytest.fixture
def person_olanormann():
    return load_json_file("cerebrum_ntnu/olanormann.json")


@pytest.fixture
def scim_uib_ri_query_single_user_page1():
    # Page 1 of a response with 2 instances of the same no:edu:scim:user.
    # One is active the other is not
    return load_json_file("scim_uib_ri/single_user_query_page1.json")


@pytest.fixture
def scim_uib_ri_query_single_user_page2():
    # Page 2 of a response with 2 instances of the same no:edu:scim:user.
    # One is active the other is not
    return load_json_file("scim_uib_ri/single_user_query_page2.json")


@pytest.fixture
def scim_uib_ri_query_single_user_multiple_active():
    # A response with 2 instances of the same no:edu:scim:user.
    # Both are active
    return load_json_file("scim_uib_ri/single_user_multiple_active_query.json")


@pytest.fixture
def scim_uib_ri_query_single_user_not_active():
    return load_json_file("scim_uib_ri/single_user_not_active_query.json")


@pytest.fixture
def scim_uib_ri_query_multiple_users():
    return load_json_file("scim_uib_ri/multiple_users_query.json")


@pytest.fixture
def scim_uib_ri_query_no_matches():
    return load_json_file("scim_uib_ri/no_matches_query.json")


@pytest.fixture
def scim_uib_ri_user_1(scim_uib_ri_query_single_user_page1):
    return scim_uib_ri_query_single_user_page1["Resources"][0]


@pytest.fixture
def scim_uib_ri_iga_result_1():
    return IgaResult(**load_json_file("scim_uib_ri/iga_result_1.json"))


@pytest.fixture
def scim_uib_ri_iga_person_1():
    return IgaPerson(**load_json_file("scim_uib_ri/iga_person_1.json"))


@pytest.fixture
def scim_uib_ri_user_3(scim_uib_ri_query_multiple_users):
    return scim_uib_ri_query_multiple_users["Resources"][2]


@pytest.fixture
def scim_uib_ri_not_found():
    return load_json_file("scim_uib_ri/not_found.json")


@pytest.fixture
def scim_uib_ri_unknown_scim_error():
    return load_json_file("scim_uib_ri/unknown_scim_error.json")


@pytest.fixture
def scim_uit_ri_query_single_user_page1():
    # Page 1 of a response with 2 instances of the same no:edu:scim:user.
    # One is active the other is not
    return load_json_file("scim_uit_ri/single_user_query_page1.json")


@pytest.fixture
def scim_uit_ri_query_single_user_page2():
    # Page 2 of a response with 2 instances of the same no:edu:scim:user.
    # One is active the other is not
    return load_json_file("scim_uit_ri/single_user_query_page2.json")


@pytest.fixture
def scim_uit_ri_query_single_user_multiple_active():
    # A response with 2 instances of the same no:edu:scim:user.
    # Both are active
    return load_json_file("scim_uit_ri/single_user_multiple_active_query.json")


@pytest.fixture
def scim_uit_ri_query_single_user_not_active():
    return load_json_file("scim_uit_ri/single_user_not_active_query.json")


@pytest.fixture
def scim_uit_ri_query_multiple_users():
    return load_json_file("scim_uit_ri/multiple_users_query.json")


@pytest.fixture
def scim_uit_ri_query_no_matches():
    return load_json_file("scim_uit_ri/no_matches_query.json")


@pytest.fixture
def scim_uit_ri_user_1(scim_uit_ri_query_single_user_page1):
    return scim_uit_ri_query_single_user_page1["Resources"][0]


@pytest.fixture
def scim_uit_ri_iga_result_1():
    return IgaResult(**load_json_file("scim_uit_ri/iga_result_1.json"))


@pytest.fixture
def scim_uit_ri_iga_person_1():
    return IgaPerson(**load_json_file("scim_uit_ri/iga_person_1.json"))


@pytest.fixture
def scim_uit_ri_user_3(scim_uit_ri_query_multiple_users):
    return scim_uit_ri_query_multiple_users["Resources"][2]


@pytest.fixture
def scim_uit_ri_not_found():
    return load_json_file("scim_uit_ri/not_found.json")


@pytest.fixture
def scim_uit_ri_unknown_scim_error():
    return load_json_file("scim_uit_ri/unknown_scim_error.json")


@pytest.fixture
def affiliations_308():
    return load_json_file("cerebrum_uio/affiliations_308.json")


@pytest.fixture
def ous_308():
    return load_json_file("cerebrum_uio/ous_308.json")


@pytest.fixture
def groups_alice():
    return load_json_file("cerebrum_uio/groups_alice.json")


@pytest.fixture
def group_members_testgroup():
    return load_json_file("cerebrum_uio/group_members_testgroup.json")


@pytest.fixture
def mock_cerebrum_uio_client(
    account_veralf2,
    person_308,
    external_ids_308,
    external_id_search_result_10137041,
    person_accounts_308,
    affiliations_308,
    ous_308,
    groups_alice,
    group_members_testgroup,
):
    accounts = {"veralf2": account_veralf2}
    person_primary_accs = {308: account_veralf2}
    persons = {308: person_308}
    person_accounts = {308: person_accounts_308}
    external_ids = {308: external_ids_308}
    affiliations = {308: affiliations_308}
    gr_alice = groups_alice

    class MockCerebrumUioClient:
        def get_account(self, person_id: str) -> Optional[Account]:
            account = accounts.get(person_id)
            if account:
                owner = OwnerRef(**account["owner"])
                account = Account(**account)
                account.owner = owner
                return account
            return None

        def get_person(self, person_id: int) -> Optional[Person]:
            person = persons.get(person_id)
            if person:
                person_result = Person(**person)
                return person_result
            return None

        def get_person_external_ids(
            self, person_id: int
        ) -> Optional[Iterator[PersonExternalId]]:
            external_id_data = external_ids.get(person_id)
            if external_id_data:
                for x in external_id_data["external_ids"]:
                    yield PersonExternalId.from_dict(x)
            return None

        def search_person_external_ids(
            self,
            source_system: Any = None,
            id_type: Any = None,
            external_id: Any = None,
        ):
            if external_id is None:
                return map(
                    PersonExternalIdSearchResult.from_dict,
                    external_id_search_result_10137041["external_ids"],
                )
            for x in external_id_search_result_10137041["external_ids"]:
                if (
                    (external_id is None or x["external_id"] == external_id.__str__())
                    and (
                        source_system is None
                        or x["source_system"] == source_system.__str__()
                    )
                    and (id_type is None or x["id_type"] == id_type.__str__())
                ):
                    yield PersonExternalIdSearchResult.from_dict(x)

        def get_person_accounts(self, employee_nr: Any):
            accounts_data = person_accounts.get(employee_nr)
            if accounts_data:
                for x in accounts_data["accounts"]:
                    yield AccountRef.from_dict(x)
            return None

        def get_person_affiliations(
            self, person_id: int
        ) -> Optional[Iterator[Affiliation]]:
            affiliation_data = affiliations.get(person_id)
            if affiliation_data:
                for x in affiliation_data["affiliations"]:
                    yield Affiliation.from_dict(x)
            return None

        def get_ou(self, affiliations: Iterator[Affiliation]):
            ous_data = ous_308
            if ous_data:
                ou = OU(**ous_data)
                return ou
            return None

        def get_person_primary_account(self, person_id) -> Optional[Account]:
            accounts_data = person_primary_accs.get(person_id)
            if accounts_data:
                return Account(**accounts_data)
            return None

        def get_account_memberships(self, name):
            """Gets alice groups"""
            resp = gr_alice
            if not name == "alice":
                return None
            return (GroupListItem.from_dict(x) for x in resp.get("groups", []))

        def list_group_members(self, group_name):
            resp = group_members_testgroup
            if group_name == "testgroup":
                for x in resp["members"]:
                    yield GroupMember(**x)

    return MockCerebrumUioClient()


@pytest.fixture
def iga_cerebrum_uio_init_kwargs():
    return dict(
        iga_impl="cerebrum@uio",
        url="http://localhost/",
        headers={"X-Gravitee-Api-Key": "abababab-abab-abab-abab-abababababab"},
    )


@pytest.fixture
def iga_cerebrum_uio(mock_cerebrum_uio_client, iga_cerebrum_uio_init_kwargs):
    iga_client = IgaAbstract.get_iga_client(
        parse_client_config(**iga_cerebrum_uio_init_kwargs)
    )

    assert type(iga_client) == abstract_iga_client.iga_cerebrum_uio.IgaCerebrumUio
    assert iga_client.client.urls.baseurl == "http://localhost/"
    assert (
        iga_client.client.headers["X-Gravitee-Api-Key"]
        == "abababab-abab-abab-abab-abababababab"
    )

    iga_client.client = mock_cerebrum_uio_client
    return iga_client


@pytest.fixture
def mock_ntnu_tia_api(mock_api, person_olanormann):
    matcher_404 = re.compile(r"http://localhost/person/")
    mock_api.register_uri("GET", matcher_404, status_code=404)
    keys = person_olanormann["person"]["bas2_person"]["keys"]
    for k in KeyTypeEnum:
        if k in keys:
            value = keys[k]
            mock_api.get(
                f"http://localhost/person/{k}/{value}/?fields=person.bas2_person",
                json=person_olanormann,
            )


@pytest.fixture
def iga_cerebrum_ntnu_init_kwargs():
    return dict(
        iga_impl="cerebrum@ntnu",
        url="http://localhost/",
        headers={"a": "b"},
    )


@pytest.fixture
def iga_cerebrum_ntnu(mock_ntnu_tia_api, iga_cerebrum_ntnu_init_kwargs):
    iga_client = IgaAbstract.get_iga_client(
        parse_client_config(**iga_cerebrum_ntnu_init_kwargs)
    )

    assert type(iga_client) == abstract_iga_client.iga_cerebrum_ntnu.IgaCerebrumNtnu
    assert iga_client.client.urls.baseurl == "http://localhost/"
    assert iga_client.client.headers["a"] == "b"

    return iga_client


@pytest.fixture(params=(True, False))
def iga_scim_uib_ri_init_kwargs(request):
    return dict(
        iga_impl="scim-ri@uib",
        endpoints=dict(base_url="http://localhost.localdomain"),
        headers={"X-Gravitee-Api-Key": "a-key"},
        can_use_complex_filters=request.param,
    )


@pytest.fixture
def iga_scim_uib_ri(
    mock_api, iga_scim_uib_ri_init_kwargs
) -> abstract_iga_client.iga_scim_ri_uib.IgaClient:
    config = parse_client_config(**iga_scim_uib_ri_init_kwargs)
    iga_client = IgaAbstract.get_iga_client(config=config)

    assert isinstance(iga_client, abstract_iga_client.iga_scim_ri_uib.IgaClient)
    assert iga_client.client.config.endpoints.base_url == "http://localhost.localdomain"
    assert iga_client.client.config.headers["X-Gravitee-Api-Key"] == SecretStr("a-key")

    return iga_client


@pytest.fixture(params=(True, False))
def iga_scim_uit_ri_init_kwargs(request):
    return dict(
        iga_impl="scim-ri@uit",
        endpoints=dict(base_url="http://localhost.localdomain"),
        headers={"X-Gravitee-Api-Key": "a-key"},
        can_use_complex_filters=request.param,
    )


@pytest.fixture
def iga_scim_uit_ri(
    mock_api, iga_scim_uit_ri_init_kwargs
) -> abstract_iga_client.iga_scim_ri_uit.IgaClientUit:
    config = parse_client_config(**iga_scim_uit_ri_init_kwargs)
    iga_client = IgaAbstract.get_iga_client(config=config)

    assert isinstance(iga_client, abstract_iga_client.iga_scim_ri_uit.IgaClientUit)
    assert iga_client.client.config.endpoints.base_url == "http://localhost.localdomain"
    assert iga_client.client.config.headers["X-Gravitee-Api-Key"] == SecretStr("a-key")

    return iga_client


@pytest.fixture
def account_alice():
    return load_json_file("cerebrum_uio/account_alice.json")


@pytest.fixture
def person_alice_accounts():
    return load_json_file("cerebrum_uio/person_alice_accounts.json")


@pytest.fixture
def person_alice_accounts_no_primary(person_alice_accounts):
    return {
        "accounts": [x for x in person_alice_accounts["accounts"] if not x["primary"]]
    }


@pytest.fixture
def person_alice_externalids():
    return load_json_file("cerebrum_uio/person_alice_externalids.json")


@pytest.fixture
def person_alice():
    return load_json_file("cerebrum_uio/person_alice.json")


@pytest.fixture
def person_alice_contacts():
    return load_json_file("cerebrum_uio/person_alice_contacts.json")


@pytest.fixture
def igaperson_alice():
    return load_json_file("cerebrum_uio/igaperson_alice.json")


@pytest.fixture
def igaperson_alice_no_primary_accounts(igaperson_alice):
    person = IgaPerson(**igaperson_alice)
    person.accounts = [x for x in (person.accounts or ()) if x.type != "primary"]
    person.primary_email = None
    person.feide_id = None
    return person


@pytest.fixture
def account_alice_drift():
    return load_json_file("cerebrum_uio/account_alice-drift.json")


@pytest.fixture
def mock_api():
    with requests_mock.Mocker() as m:
        yield m
