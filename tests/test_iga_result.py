import inspect
import pydantic
import pytest
from abstract_iga_client import iga_result


@pytest.fixture
def model_classes():
    classes = []
    for name, obj in inspect.getmembers(iga_result):
        if inspect.isclass(obj) and issubclass(obj, pydantic.BaseModel):
            classes.append(obj)
    assert len(classes) == 8
    return classes


def test_correct_superclass(model_classes):
    for cls in (x for x in model_classes if x != iga_result.BaseModel):
        assert issubclass(cls, iga_result.BaseModel)


def test_extra_not_allowed(model_classes):
    for cls in model_classes:
        assert cls.__config__.extra == pydantic.Extra.forbid


def test_validate_assignment_is_true(model_classes):
    for cls in model_classes:
        assert cls.__config__.validate_assignment is True


def test_iga_person_fail_on_invalid_assignment(igaperson_alice):
    person = iga_result.IgaPerson(**igaperson_alice)
    with pytest.raises(pydantic.ValidationError):
        person.display_name = object()  # type: ignore


def test_iga_result_fail_on_invalid_assignment(
    scim_uib_ri_iga_result_1: iga_result.IgaResult,
):
    with pytest.raises(pydantic.ValidationError):
        scim_uib_ri_iga_result_1.display_name = object()  # type: ignore
