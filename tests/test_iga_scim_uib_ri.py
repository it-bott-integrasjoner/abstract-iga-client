from typing import TypeVar

import pytest
import requests
from requests import HTTPError
from scim2_client import (
    ScimError,
    ResourceResponse,
    Client,
    QueryRequest,
    ResourceRequest,
    Equal,
    QueryParameters,
)

from abstract_iga_client.config import parse_client_config
from abstract_iga_client.iga_abstract import IgaAbstract
from abstract_iga_client.iga_result import IgaResult, IgaPerson
from abstract_iga_client.iga_scim_ri_uib import IgaClient, user_query

T = TypeVar("T")


@pytest.fixture(
    params=[
        "get_person_by_username",
        "get_person_by_employee_nr",
        "get_person_by_fs_id",
        "get_person_by_greg_person_id",
    ],
)
def get_single_iga_result_query_method_name(request):
    return request.param


@pytest.mark.parametrize(
    argnames=[
        "session",
    ],
    argvalues=[
        [None],
        [requests.Session()],
    ],
)
def test_init_with_session(session, iga_scim_uib_ri_init_kwargs):
    client = IgaAbstract.get_iga_client(
        session=session, config=parse_client_config(**iga_scim_uib_ri_init_kwargs)
    )

    assert isinstance(client, IgaClient)
    if session is None:
        assert isinstance(client.client.session, requests.Session)
    else:
        assert client.client.session is session


@pytest.mark.parametrize(
    argnames=["qualified"],
    argvalues=[
        [True],
        [False],
    ],
)
def test_get_person_by_username_unqualified(
    qualified,
    iga_scim_uib_ri: IgaClient,
    mock_api,
    scim_uib_ri_query_single_user_page1,
    scim_uib_ri_query_single_user_page2,
    scim_uib_ri_iga_result_1,
):
    if qualified:
        username = "abc@uib.no"
    else:
        username = "abc"

    class TestClient(Client):
        def __call__(self, request: ResourceRequest[T]) -> ResourceResponse[T]:
            assert isinstance(request, QueryRequest)
            assert request.parameters
            if qualified:
                expected = (
                    f'(active eq true) and (userName eq "{username}")'
                    if iga_scim_uib_ri.config.can_use_complex_filters
                    else f'userName eq "{username}"'
                )
            else:
                expected = (
                    f'(active eq true) and (userName sw "{username}@")'
                    if iga_scim_uib_ri.config.can_use_complex_filters
                    else f'userName sw "{username}@"'
                )
            assert str(request.parameters.filter) == expected
            return super().__call__(request)

    iga_scim_uib_ri.client.__class__ = TestClient
    mock_api.get(
        "http://localhost.localdomain/Users",
        json=scim_uib_ri_query_single_user_page1,
    )
    mock_api.get(
        "http://localhost.localdomain/Users?startIndex=2",
        json=scim_uib_ri_query_single_user_page2,
    )
    res = iga_scim_uib_ri.get_person_by_username(username)

    assert type(res) == IgaResult
    assert res == scim_uib_ri_iga_result_1


@pytest.fixture(params=[True, False])
def single_iga_result(
    request,
    scim_uib_ri_query_single_user_page1,
    scim_uib_ri_query_single_user_page2,
    scim_uib_ri_query_single_user_not_active,
    scim_uib_ri_iga_result_1,
    mock_api,
    iga_scim_uib_ri,
):
    is_active = request.param
    if is_active:
        mock_api.get(
            iga_scim_uib_ri.client.config.endpoints.users_url,
            json=scim_uib_ri_query_single_user_page1,
        )
        mock_api.get(
            f"{iga_scim_uib_ri.client.config.endpoints.users_url}?startIndex=2",
            json=scim_uib_ri_query_single_user_page2,
        )
        return scim_uib_ri_iga_result_1

    mock_api.get(
        iga_scim_uib_ri.client.config.endpoints.users_url,
        json=scim_uib_ri_query_single_user_not_active,
    )
    return None


def test_get_single_iga_result_by_something(
    iga_scim_uib_ri: IgaClient,
    single_iga_result,
    scim_uib_ri_iga_result_1,
    get_single_iga_result_query_method_name,
):
    res = getattr(iga_scim_uib_ri, get_single_iga_result_query_method_name)("00000001")

    if res is not None:
        assert type(res) == IgaResult
    assert res == single_iga_result


def test_get_single_iga_result_by_something_no_match(
    iga_scim_uib_ri: IgaClient,
    mock_api,
    scim_uib_ri_query_no_matches,
    get_single_iga_result_query_method_name,
):
    mock_api.get(
        iga_scim_uib_ri.client.config.endpoints.users_url,
        json=scim_uib_ri_query_no_matches,
    )

    res = getattr(iga_scim_uib_ri, get_single_iga_result_query_method_name)("00000001")

    assert res is None


def test_get_single_iga_result_by_something_multiple_matches(
    iga_scim_uib_ri: IgaClient,
    mock_api,
    scim_uib_ri_query_multiple_users,
    get_single_iga_result_query_method_name,
):
    mock_api.get(
        iga_scim_uib_ri.client.config.endpoints.users_url,
        json=scim_uib_ri_query_multiple_users,
    )

    result = getattr(iga_scim_uib_ri, get_single_iga_result_query_method_name)(
        "00000001"
    )

    assert result is None


def test_get_single_iga_result_by_something_unknown_error(
    iga_scim_uib_ri: IgaClient,
    mock_api,
    scim_uib_ri_unknown_scim_error,
    get_single_iga_result_query_method_name,
):
    mock_api.get(
        iga_scim_uib_ri.client.config.endpoints.users_url,
        json=scim_uib_ri_unknown_scim_error,
        status_code=501,
    )

    with pytest.raises(ScimError):
        getattr(iga_scim_uib_ri, get_single_iga_result_query_method_name)("00000001")


def test_get_single_iga_result_by_something_multiple_active(
    iga_scim_uib_ri: IgaClient,
    mock_api,
    scim_uib_ri_query_single_user_multiple_active,
    get_single_iga_result_query_method_name,
):
    mock_api.get(
        iga_scim_uib_ri.client.config.endpoints.users_url,
        json=scim_uib_ri_query_single_user_multiple_active,
    )

    result = getattr(iga_scim_uib_ri, get_single_iga_result_query_method_name)(
        "00000001"
    )

    assert result is None


def test_get_person_by_greg_person_id(
    iga_scim_uib_ri: IgaClient,
    mock_api,
    scim_uib_ri_query_single_user_page1,
    scim_uib_ri_query_single_user_page2,
    scim_uib_ri_iga_result_1,
):
    if iga_scim_uib_ri.config.can_use_complex_filters:
        url = "http://localhost.localdomain/Users?filter=%28active+eq+true%29+and+%28no%3Aedu%3Ascim%3Auser%3AgregPersonNumber+eq+%22666%22%29"
    else:
        url = "http://localhost.localdomain/Users?filter=no%3Aedu%3Ascim%3Auser%3AgregPersonNumber+eq+%22666%22"
    mock_api.get(url, json=scim_uib_ri_query_single_user_page1)
    mock_api.get(f"{url}&startIndex=2", json=scim_uib_ri_query_single_user_page2)

    res = iga_scim_uib_ri.get_person_by_greg_person_id(666)

    assert type(res) == IgaResult
    assert res == scim_uib_ri_iga_result_1


def test_get_person_by_id(
    iga_scim_uib_ri: IgaClient,
    mock_api,
    scim_uib_ri_user_1,
    scim_uib_ri_iga_result_1,
):
    person_id = "00000001"
    mock_api.get(
        f"{iga_scim_uib_ri.client.config.endpoints.users_url}/{person_id}",
        json=scim_uib_ri_user_1,
    )

    res = iga_scim_uib_ri.get_person_by_id(person_id)

    assert type(res) == IgaResult
    assert res == scim_uib_ri_iga_result_1


def test_get_person_by_id_not_found(
    iga_scim_uib_ri: IgaClient,
    mock_api,
    scim_uib_ri_not_found,
):
    person_id = "00000001"
    mock_api.get(
        f"{iga_scim_uib_ri.client.config.endpoints.users_url}/{person_id}",
        json=scim_uib_ri_not_found,
        status_code=404,
    )

    res = iga_scim_uib_ri.get_person_by_id(person_id)

    assert res is None


def test_get_person_by_id_unknown_scim_error(
    iga_scim_uib_ri: IgaClient, mock_api, scim_uib_ri_unknown_scim_error
):
    person_id = "00000001"
    mock_api.get(
        f"{iga_scim_uib_ri.client.config.endpoints.users_url}/{person_id}",
        json=scim_uib_ri_unknown_scim_error,
        status_code=501,
    )

    with pytest.raises(ScimError):
        iga_scim_uib_ri.get_person_by_id(person_id)


def test_get_person_by_id_unknown_not_found(
    iga_scim_uib_ri: IgaClient,
    mock_api,
):
    person_id = "00000001"
    mock_api.get(
        f"{iga_scim_uib_ri.client.config.endpoints.users_url}/{person_id}",
        json="Not Found",
        status_code=404,
    )

    with pytest.raises(HTTPError):
        iga_scim_uib_ri.get_person_by_id(person_id)


def test_get_single_iga_person_by_username(
    iga_scim_uib_ri: IgaClient,
    mock_api,
    scim_uib_ri_query_single_user_page1,
    scim_uib_ri_query_single_user_page2,
    scim_uib_ri_iga_person_1,
):
    mock_api.get(
        iga_scim_uib_ri.client.config.endpoints.users_url,
        json=scim_uib_ri_query_single_user_page1,
    )

    mock_api.get(
        f"{iga_scim_uib_ri.client.config.endpoints.users_url}?startIndex=2",
        json=scim_uib_ri_query_single_user_page2,
    )

    res = iga_scim_uib_ri.get_igaperson_by_username(
        "00000001", set(IgaPerson.__fields__.keys())
    )

    assert type(res) == IgaPerson
    assert res == scim_uib_ri_iga_person_1


def test_get_single_iga_person_by_username_not_active(
    iga_scim_uib_ri: IgaClient,
    mock_api,
    scim_uib_ri_query_single_user_not_active,
):
    mock_api.get(
        iga_scim_uib_ri.client.config.endpoints.users_url,
        json=scim_uib_ri_query_single_user_not_active,
    )

    res = iga_scim_uib_ri.get_igaperson_by_username(
        "00000001", set(IgaPerson.__fields__.keys())
    )

    assert res is None


def test_get_single_iga_person_by_username_multiple_active(
    iga_scim_uib_ri: IgaClient,
    mock_api,
    scim_uib_ri_query_single_user_multiple_active,
):
    mock_api.get(
        iga_scim_uib_ri.client.config.endpoints.users_url,
        json=scim_uib_ri_query_single_user_multiple_active,
    )

    result = iga_scim_uib_ri.get_igaperson_by_username(
        "00000001", set(IgaPerson.__fields__.keys())
    )

    assert result is None


def test_get_single_iga_person_by_username_no_match(
    iga_scim_uib_ri: IgaClient,
    mock_api,
    scim_uib_ri_query_no_matches,
):
    mock_api.get(
        iga_scim_uib_ri.client.config.endpoints.users_url,
        json=scim_uib_ri_query_no_matches,
    )

    res = iga_scim_uib_ri.get_igaperson_by_username(
        "00000001", set(IgaPerson.__fields__.keys())
    )

    assert res is None


def test_get_igaperson_by_personid(
    iga_scim_uib_ri: IgaClient,
    mock_api,
    scim_uib_ri_user_1,
    scim_uib_ri_iga_person_1,
):
    person_id = "00000001"
    mock_api.get(
        f"{iga_scim_uib_ri.client.config.endpoints.users_url}/{person_id}",
        json=scim_uib_ri_user_1,
    )

    res = iga_scim_uib_ri.get_igaperson_by_personid(
        person_id, set(IgaPerson.__fields__.keys())
    )

    assert type(res) == IgaPerson
    assert res == scim_uib_ri_iga_person_1


def test_get_igaperson_by_personid_not_active(
    iga_scim_uib_ri: IgaClient,
    mock_api,
    scim_uib_ri_user_3,
):
    person_id = "00000001"
    mock_api.get(
        f"{iga_scim_uib_ri.client.config.endpoints.users_url}/{person_id}",
        json=scim_uib_ri_user_3,
    )

    res = iga_scim_uib_ri.get_igaperson_by_personid(
        person_id, set(IgaPerson.__fields__.keys())
    )

    assert res is None


def test_get_igaperson_by_personid_fields_to_include(
    iga_scim_uib_ri: IgaClient,
    mock_api,
    scim_uib_ri_user_1,
    scim_uib_ri_iga_person_1,
):
    person_id = "00000001"
    mock_api.get(
        f"{iga_scim_uib_ri.client.config.endpoints.users_url}/{person_id}",
        json=scim_uib_ri_user_1,
    )

    res = iga_scim_uib_ri.get_igaperson_by_personid(person_id, {"feide_id"})

    assert type(res) == IgaPerson
    assert res == scim_uib_ri_iga_person_1


def test_get_igaperson_by_personid_not_found(
    iga_scim_uib_ri: IgaClient,
    mock_api,
    scim_uib_ri_not_found,
):
    person_id = "00000001"
    mock_api.get(
        f"{iga_scim_uib_ri.client.config.endpoints.users_url}/{person_id}",
        json=scim_uib_ri_not_found,
        status_code=404,
    )

    res = iga_scim_uib_ri.get_igaperson_by_personid(
        person_id, set(IgaPerson.__fields__.keys())
    )

    assert res is None


def test_get_groups(iga_scim_uib_ri: IgaClient, mock_api):
    with pytest.raises(NotImplementedError):
        # Write a proper test if and when method is implemented
        username = "username"
        iga_scim_uib_ri.get_groups(username)


def test_get_all_persons(iga_scim_uib_ri: IgaClient, mock_api):
    with pytest.raises(NotImplementedError):
        # Write a proper test if and when method is implemented
        iga_scim_uib_ri.get_all_persons()


def test_get_all_employees(
    iga_scim_uib_ri: IgaClient,
    mock_api,
    scim_uib_ri_query_multiple_users,
    scim_uib_ri_query_no_matches,
    scim_uib_ri_iga_result_1,
):
    if iga_scim_uib_ri.config.can_use_complex_filters:
        url = "http://localhost.localdomain/Users?filter=(active eq true) and (no:edu:scim:user:employeeNumber pr)&attributes=id,name,active,emails,phoneNumbers,roles,meta.resourceType,no:edu:scim:user:accountType,userName,no:edu:scim:user:employeeNumber,no:edu:scim:user:fsPersonNumber,no:edu:scim:user:eduPersonPrincipalName,no:edu:scim:user:gregPersonNumber"
    else:
        url = "http://localhost.localdomain/Users?filter=no:edu:scim:user:employeeNumber pr&attributes=id,name,active,emails,phoneNumbers,roles,meta.resourceType,no:edu:scim:user:accountType,userName,no:edu:scim:user:employeeNumber,no:edu:scim:user:fsPersonNumber,no:edu:scim:user:eduPersonPrincipalName,no:edu:scim:user:gregPersonNumber"
    mock_api.get(
        url,
        json=scim_uib_ri_query_multiple_users,
        complete_qs=True,
    )
    xs = iga_scim_uib_ri.get_all_employees()

    assert isinstance(xs, list)
    assert len(xs) == 2
    assert xs[0] == scim_uib_ri_iga_result_1


def test_get_person_consents(iga_scim_uib_ri: IgaClient, mock_api):
    with pytest.raises(NotImplementedError):
        # Write a proper test if and when method is implemented
        person_id = "00000001"
        iga_scim_uib_ri.get_person_consents(person_id)


@pytest.mark.parametrize("can_use_complex_filters", (True, False))
def test_user_query_no_extra_compare(can_use_complex_filters):
    query = user_query(
        count=3,
        start_index=4,
        attr_expr=None,
        can_use_complex_filters=can_use_complex_filters,
    )

    assert isinstance(query.parameters, QueryParameters)
    assert str(query.parameters.filter) == "active eq true"
    assert query.parameters.count == 3
    assert query.parameters.start_index == 4


@pytest.mark.parametrize("can_use_complex_filters", (True, False))
def test_user_query_complex_filters_extra_compare(can_use_complex_filters):
    compare = Equal(name="fieldName", value="OK")
    query = user_query(
        count=3,
        start_index=4,
        attr_expr=compare,
        can_use_complex_filters=can_use_complex_filters,
    )

    assert isinstance(query.parameters, QueryParameters)
    if can_use_complex_filters:
        assert (
            str(query.parameters.filter) == '(active eq true) and (fieldName eq "OK")'
        )
    else:
        assert str(query.parameters.filter) == 'fieldName eq "OK"'
    assert query.parameters.count == 3
    assert query.parameters.start_index == 4
